# -*- coding: utf-8 -*-
#import config
import telebot
import socket
import socks
#from telebot import apihelper


from PIL import Image, ExifTags
from scipy.misc import imresize
import numpy as np
from keras.models import load_model
import tensorflow as tf


ip = '85.25.207.105'  # change your proxy's ip
port =  61116# change your proxy's port
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, ip, port)
socket.socket = socks.socksocket
token = '590444239:AAHifqQ2cIRvwetvJGWWWu1Uw80Fk08rfQo'

#apihelper.proxy = {
 # 'http', 'socks5://login:pass@85.25.207.105:61116',
  #'https', 'socks5://login:pass@85.25.207.105:61116'
#}

bot = telebot.TeleBot(token)

model = load_model('./model/main_model.hdf5', compile=False)
graph = tf.get_default_graph()

def ml_predict(image):
    with graph.as_default():
        # Add a dimension for the batch
        prediction = model.predict(image[None, :, :, :])
    prediction = prediction.reshape((224,224, -1))
    return prediction
    

def rotate_by_exif(image):
    try :
        for orientation in ExifTags.TAGS.keys() :
            if ExifTags.TAGS[orientation]=='Orientation' : break
        exif=dict(image._getexif().items())
        if not orientation in exif:
            return image

        if   exif[orientation] == 3 :
            image=image.rotate(180, expand=True)
        elif exif[orientation] == 6 :
            image=image.rotate(270, expand=True)
        elif exif[orientation] == 8 :
            image=image.rotate(90, expand=True)
        return image
    except:
        return image

THRESHOLD = 0.5    


@bot.message_handler(content_types=["text"])
def repeat_all_messages(message): # Название функции не играет никакой роли, в принципе
    bot.send_message(message.chat.id, "Загрузите фото для обработки!")
    
@bot.message_handler(content_types=['photo'])
def photo(message):
    file_id = message.photo[2].file_id
    newFile = bot.get_file(file_id)
    downloaded_file = bot.download_file(newFile.file_path)
    with open('new_file.jpg', 'wb') as new_file:
         new_file.write(downloaded_file)
    #img = Image.open('1.png')
    f = open('1.png', 'r')
    read_data = f.read()
    
    
    image = Image.open('new_file.jpg')
    image = rotate_by_exif(image)
    resized_image = imresize(image, (224, 224)) / 255.0

    # Model input shape = (224,224,3)
    # [0:3] - Take only the first 3 RGB channels and drop ALPHA 4th channel in case this is a PNG
    prediction = ml_predict(resized_image[:, :, 0:3])
    print('PREDICTION COUNT', (prediction[:, :, 1]>0.5).sum())

    # Resize back to original image size
    # [:, :, 1] = Take predicted class 1 - currently in our model = Person class. Class 0 = Background
    prediction = imresize(prediction[:, :, 1], (image.height, image.width))
    prediction[prediction>THRESHOLD*255] = 255
    prediction[prediction<THRESHOLD*255] = 0

    # Append transparency 4th channel to the 3 RGB image channels.
    transparent_image = np.append(np.array(image)[:, :, 0:3], prediction[: , :, None], axis=-1)
    #transparent_image = Image.fromarray(transparent_image)
    
    transparent_image = Image.fromarray(transparent_image)
   
    width = 132
    ratio = (width / float(transparent_image.size[0]))
    height = int(float(transparent_image.size[1]) * float(ratio))
    transparent_image = transparent_image.resize((width, height)) 

    # Send back the result image to the client
    #byte_io = io.BytesIO()
    #transparent_image.save(byte_io, 'PNG')
    res_img = Image.new("RGB", (396, 360), (255, 255, 255))
    res_img.paste(transparent_image, (0, 0), transparent_image)
    res_img.paste(transparent_image, (132, 0), transparent_image)
    res_img.paste(transparent_image, (264, 0), transparent_image)
    res_img.paste(transparent_image, (0, 170), transparent_image)
    res_img.paste(transparent_image, (132, 170), transparent_image)
    res_img.paste(transparent_image, (264, 170), transparent_image)
    with open('res_file.png', 'wb') as image_file:
        res_img.save(image_file, 'PNG')
        image_file.close()
    #byte_io.seek(0)
    
    
    
    #bot.send_message(message.chat.id, "hui")
    bot.send_photo(message.chat.id, photo=open('res_file.png', 'r')) 

if __name__ == '__main__':
     bot.polling(none_stop=True)
