class Config(object):
    SITE_NAME='NeuroPOD'
    SITE_SLUG_NAME='Photo On Documents'
    SITE_LOCATION='Outer space'
    TAGLINE='Photo On Documents'
    TAGLINES=['Machine learning for creating photo on documents']
    SITE_DESCRIPTION='Photo on documents'
    SITE_KEYWORDS='machine learning, photo on documents'
    GOOGLE_SITE_VERIFICATION=' X'
    FACEBOOK_PAGE_ID=''
    TWITTER_ID=''
    #  google_plus_id='X'
    GOOGLE_ANALYTICS=' UA-x'
    ADMINS=['ivfedorov1242@gmail.com']


class DevelopmentConfig(Config):
    DOMAIN= 'localhost=5000'
    ASSET_DOMAIN= 'localhost=5000'

class ProductionConfig(Config):
    DOMAIN= 'www.neuropod.herokuapp.com'
    ASSET_DOMAIN= 'www.neuropod.herokuapp.com'
